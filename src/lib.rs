extern crate dolphin;

use dolphin::*;

use std::collections::HashMap;
use std::net::Ipv4Addr;

pub struct PassiveDns {
    data: HashMap<Ipv4Addr, Vec<dns::Name>>
}

impl PassiveDns {
    pub fn new() -> PassiveDns {
        PassiveDns {
            data: HashMap::new()
        }
    }

    pub fn handle_packet(&mut self, data: &[u8]) -> (&[dns::Name], &[dns::Name]) {
        let ether_packet = match ethernet::Packet::parse(data) {
            IResult::Done(_, packet) => packet,
            _ => return  (&[], &[])
        };

        if ether_packet.ether_type > 1500 && ether_packet.ether_type != 0x800 {
            // Not a Ipv4 packet, just skip it.
            return  (&[], &[]);
        }

        let ip_packet = match ipv4::Packet::parse(ether_packet.payload) {
            IResult::Done(_, packet) => packet,
            _ => return (&[], &[])
        };

        self.add_to_dns(&ip_packet);
        let source_names = self.data.get(&ip_packet.source_address).map(|x| &x[..]).unwrap_or(&[]);
        let dest_names =  self.data.get(&ip_packet.destination_address).map(|x| &x[..]).unwrap_or(&[]);
        (source_names, dest_names)
    }

    fn add_to_dns(&mut self, ip_packet: &ipv4::Packet) {
        if ip_packet.protocol != 17 {
            // Not a udp packet, just skip it
            return;
        }

        let udp_packet = match udp::Packet::parse(ip_packet.payload) {
            IResult::Done(_, packet) => packet,
            _ => return
        };

        let dns_packet = match dns::Packet::parse(udp_packet.payload) {
            IResult::Done(_, packet) => packet,
            _ => return
        };

        if dns_packet.response_code != 0 {
            return
        }

        for rr in dns_packet.answers {
            if rr.type_ != 1 || rr.class != 1 || rr.data.len() != 4 {
                return
            }

            // Manually parse IP address
            let ip = Ipv4Addr::new(rr.data[0], rr.data[1], rr.data[2], rr.data[3]);

            let domains = self.data.entry(ip).or_insert(vec![]);
            if !domains.contains(&rr.name) {
                println!("Adding {} -> {:?}", ip, rr.name);
                domains.push(rr.name);
            }
        }
    }
}